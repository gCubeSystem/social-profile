
# Changelog for Social Profile portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.4.0] - 2020-12-14

 - Removed the import from linked in button

## [v2.2.0] - 2018-06-08

- Fix for Incident #11878: Import profile from LinkedIn error added

- Refactored for to Java8 and GWT 2.8.2

- Removed ws mail widget dependency and implement private message through Messages App Redirect

## [v2.0.0] - 2016-05-01

Moved to Liferay 6.2

Background summary editable directly

## [v1.1.0] - 2015-01-07

Fixed import from LinkedIn problem when headline longer than 75 chars

## [v1.0.0] - 2014-11-22

Added LinkedIn profile import feature

## [v0.1.0] - 2013-01-13

First release 
